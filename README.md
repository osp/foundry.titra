### Historical background

To Do

### Context

To Do

## Content of the directory

- Bold (Int and Ext)
    - OTF files
    - bmap Mac fontfile
- Light (Int and Ext)
    - OTF files
    - bmap Mac fontfile

- Fontlog

## Known issues and future developments

- Bad encoding so for now your computer can't make the difference between the bold and light version

## Coverage

- Titra Bold currently provides the following Unicode coverage:
    - Basic Latin: 95/95 
    - Latin-1 Supplement: 94/96 
    - Latin extended A: 10/128

- Titra Light currently provides the following Unicode coverage:
    - Basic Latin: 95/95 
    - Latin-1 Supplement: 95/96 
    - Latin extended A: 10/128

## Information for Contributors

Copyright (c) 2000 Hammerfonts

## ChangeLog

To do

## Acknowledgements

If you make modifications be sure to add your name (N), email (E), web-address (W) and description (D).  
This list is sorted by last name in alphabetical order. 

N: Pierre Huyghebaert  
E: pierre@speculoos.com<br/>
W: http://www.speculoos.com<br/>
D: Typography